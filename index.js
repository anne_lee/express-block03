const express = require('express'),
app = express(),
mongoose = require('mongoose'),
bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

// ## EXERCISE
// /categories/add      POST    ===>    add new category to DB
// /categories/remove   POST    ===>    remove category from DB
// /categories/update   POST    ===>    update category
// /categories          GET     ===>    GET all categories
// /products/add        POST    ===>    add new product to DB
// /products/remove     POST    ===>    remove product from DB
// /products/update     POST    ===>    update product
// /products            GET     ===>    get all products
// /products/product    POST    ===>    get one product by title or id passed in the body 
// /products/:category  GET     ===>    get all products that belongs to a specific category

async function connecting(){
  try {
    await mongoose.connect('mongodb://127.0.0.1/anotherdatabase', { useUnifiedTopology: true , useNewUrlParser: true })
    console.log('Connected to the DB')
  } catch ( error ) {
    console.log('ERROR: Seems like your DB is not running, please start it up !!!');
  }
}
connecting()

mongoose.set('useCreateIndex', true);

app.use('/categories', require('./routes/catRoutes'));
app.use('/products', require('./routes/proRoutes'))

app.listen(3002, () => console.log(`listening on port 3002`))
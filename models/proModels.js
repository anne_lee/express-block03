const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const proSchema = new Schema({
  product: {
    type: String,
    required: true,
    unique: true
  },
  category: {
    type: String,
    required: true
  },
  price: {
    type: Number,
    required: true
  }
})
module.exports = mongoose.model('products', proSchema);
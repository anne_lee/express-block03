const Categories = require('../models/catModels.js');

 
const insert = async(req, res) => {
  const {category} = req.body;
  try{
    const added = await Categories.create({
        category: category,
        products: []
    })
    res.send({succ: true, added: added})
  } catch (error) {
    res.send({succ: false, error: error})
  }
}

const remove = async(req, res) => {
  const {category} = req.body;
  try{
    const deleted = await Categories.deleteOne({
      category: category
    })
    res.send({succ: true, deleted: deleted})
  } catch (error) {
    res.send({succ: false, error: error})
  }
}

const update = async(req, res) => {
  const {oldCategory, newCategory} = req.body;
  try{
    const updated = await Categories.updateOne(
      {category: oldCategory}, {category: newCategory}
    )
    res.send({succ: true, updated: updated})
  }catch (error) {
    res.send({succ:false, error: error})
  }
}

const findAll = async(req, res) => {
  try{
    const categories = await Categories.find();
    res.send({succ: true, categories: categories})
  }catch(error){
    res.send({succ: false, error: error})
  }
}


module.exports = {
  insert,
  remove,
  update,
  findAll
}
const Products = require('../models/proModels.js');

const insert = async(req, res) => {
  const {category,product,price} = req.body;
  try{
    const added = await Products.create({
        product: product,
        price: price,
        category: category
    })
    res.send({succ: true, added: added})
  } catch (error) {
    res.send({succ: false, error: error})
  }
}

const remove = async(req, res) => {
  const {product} = req.body;
  try{
    const removed = await Products.deleteOne({
      product: product
    })
    res.send({succ: true, removed: removed})
  }catch(error){
    res.send({succ: false, error: error})
  }
}

const update = async (req, res) => {
  const {oldProduct, newProduct, newPrice} = req.body;
  try{
    const updated = await Products.updateOne(
      {product: oldProduct}, {product: newProduct, price: newPrice}
      )
      res.send({succ: true, updated: updated})
  }catch(error){
    res.send({succ: false, error: error})
  }
}

const findAll = async (req, res) => {
  try{
    const products = await Products.find();
    res.send({succ: true, products: products})
  }catch(error){
    res.send({succ: false, error: error})
  }
    console.log(anotherdatabase)
}

const findOne = async (req, res) => {
  const {key, value} = req.body;     // _id || product
  try{
    const oneProduct = await Products.findOne({
      [key]: value
    })
    res.send({succ: true, oneProduct: oneProduct})
  }catch(error){
    res.send({succ: false, error: error})
  }
}

const findAllInCat = async (req, res) => {
  const {category} = req.params;
  try{
    const products = await Products.find({
      category: category
    });
    res.send({succ:true, products: products})
  }catch(error){
    res.send({succ: false, error: error})
  }
}


module.exports = {
  insert,
  remove,
  update,
  findAll,
  findOne,
  findAllInCat
}
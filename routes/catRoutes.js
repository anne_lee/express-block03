const express     = require('express'), 
    router        = express.Router(),
    controller    = require('../controllers/catControllers.js');


router.post('/add', controller.insert);
router.post('/remove', controller.remove)
router.post('/update', controller.update)
router.get('/', controller.findAll)


module.exports = router;

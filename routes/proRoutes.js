const express     = require('express'), 
    router        = express.Router(),
    controller    = require('../controllers/proControllers.js');

router.post('/add', controller.insert)
router.post('/remove', controller.remove)
router.post('/update', controller.update)
router.get('/', controller.findAll)
router.post('/product', controller.findOne)
router.get('/:category', controller.findAllInCat)

module.exports = router;
